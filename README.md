<!--
SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# archlinux.grub

An [Ansible](https://www.ansible.com/) role to install and configure [grub](https://www.gnu.org/software/grub/).

## Requirements

An Arch Linux system or an Arch Linux chroot.

## Role Variables

By default this role does nothing, if `grub` is unset.
The below variables define the configuration options for the [simple
configuration
handling](https://www.gnu.org/software/grub/manual/grub/grub.html#Simple-configuration)
using
[grub-mkconfig](https://www.gnu.org/software/grub/manual/grub/grub.html#Invoking-grub_002dmkconfig).

* `bootloader_type`: a string defining which type of bootloader to install (currently only `legacy` - BIOS - is supported, which is also the default)
* `grub`: a dict to define settings for grub (unset by default)
  * `default`: a string indicating which boot entry to select as default (defaults to `0`). Defaults can be provided using numbers and full strings and the special entry `saved` (see `grub.savedefault`). **NOTE**: When setting `grub.disable_submenu` to `true` the default entry has to be provided by its *full name* (e.g. `Arch Linux>Arch Linux default`).
  * `timeout`: how many seconds to wait until booting into a selection (defaults to `5`)
  * `distributor`: the distribution shown in the grub header (defaults to `Arch`)
  * `cmdline_linux_default`: a list of kernel cmdline entries to add to all Linux targets by default (defaults to `['loglevel=3', 'quiet']`)
  * `cmdline_linux`: the kernel cmdline for Linux targets (defaults to `[]`)
  * `preload_modules`: a list of modules to preload with grub (defaults to `['part_gpt', 'part_msdos']`)
  * `enable_cryptodisk`: a boolean indicating whether an encrypted root is used (defaults to `false`)
  * `timeout_style`: a string defining which style of timeout behavior to use (defaults to `menu`, other valid options are `countdown` or `hidden`)
  * `terminal_input`: a boolean indicating whether to use the console as input (defaults to `true`)
  * `terminal_output`: a boolean indicating whether to use the console as output (defaults to `false`)
  * `gfxmode`: the resolution to use for graphical terminal (defaults to `auto`)
  * `gfxpayload_linux`: a boolean indicating whether to allow the kernel to keep the same resolution as grub's (defaults to `true`)
  * `disable_linux_uuid`: a boolean indicating whether not to use UUID based devices in the kernel cmdline (defaults to `false`)
  * `disable_recovery`: a boolean indicating whether to not generate recovery mode menu entries (defaults to `true`)
  * `color_normal`: the foreground/background menu colors, provided as a string (defaults to unset)
  * `color_highlight`: the foreground/background menu colors for highlighted items, provided as a string (defaults to unset)
  * `background`: the path to a background image (defaults to unset)
  * `theme`: the path to a theme (defaults to unset)
  * `init_tune`: a string describing a start sound using three numbers (defaults to unset)
  * `savedefault`: a boolean indicating whether to make the last selection the default (defaults to `false`). **NOTE**: For this to work, `grub.default` has to be set to `saved`!
  * `disable_submenu`: a boolean indicating whether to not use a submenu per Operating System (defaults to `false`)
  * `disable_os_prober`: a boolean indicating whether not to use os-prober (defaults to `true` - for security reasons)
  * `system_disks`: a list of strings defining absolute paths of block devices to install grub to when targeting legacy systems (defaults to `[]`)

The following variables have no defaults in this role, but are available to override behavior and use special functionality:

* `chroot_dir`: the directory to chroot into, when including `chroot.yml` tasks (defaults to unset)
* `kernel_cmdline`: a list of strings representing kernel cmdline options, which are added after `grub.cmdline_linux_default` when creating `/etc/default/grub` (defaults to `[]`)

## Dependencies

None

## Example Playbook

1) Configure grub with custom kernel commandline in a chroot

```yaml
- name: Configure grub in a chroot with custom kernel cmdline and timeout
  hosts: some_hosts_in_chroot
  vars:
    chroot_dir: /
    grub:
      system_disks:
        - /dev/vda
      timeout: 3
    kernel_cmdline:
      - foo
      - bar
  include_role:
    name: archlinux.grub
    tasks_from: chroot
```

## License

GPL-3.0-or-later

## Author Information

David Runge <dvzrv@archlinux.org>
